// Adding VPC Networks to Project  MANAGEMENT
resource "google_compute_subnetwork" "management-sub" {
  name          = "management-sub"
  ip_cidr_range = "10.0.0.0/24"
  network       = google_compute_network.management.self_link
  region        = var.region
}

resource "google_compute_network" "management" {
  name                    = var.interface_0_name
  auto_create_subnetworks = "false"
}

// Adding VPC Networks to Project  UNTRUST
resource "google_compute_subnetwork" "untrust-sub" {
  name          = "untrust-sub"
  ip_cidr_range = "10.0.1.0/24"
  network       = google_compute_network.untrust.self_link
  region        = var.region
}

resource "google_compute_network" "untrust" {
  name                    = var.interface_1_name
  auto_create_subnetworks = "false"
}

// Adding VPC Networks to Project  TRUST
resource "google_compute_subnetwork" "trust-sub" {
  name          = "trust-sub"
  ip_cidr_range = "10.0.2.0/24"
  network       = google_compute_network.trust.self_link
  region        = var.region
}

resource "google_compute_network" "trust" {
  name                    = var.interface_2_name
  auto_create_subnetworks = "false"
}
