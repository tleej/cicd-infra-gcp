// Adding GCP Route to TRUST Interface
//resource "google_compute_route" "trust" {
//  name                   = "trust-route"
//  dest_range             = "0.0.0.0/0"
//  network                = google_compute_network.trust.self_link
//  next_hop_instance      = element(google_compute_instance.firewall.*.name, 0)
//  next_hop_instance_zone = var.zone
//  priority               = 100
//
//  depends_on = [
//    google_compute_instance.firewall,
//    google_compute_network.trust,
//    google_compute_network.untrust,
//    google_compute_network.management,
//    google_container_cluster.kubernetes
//  ]
//}



// Adding GCP Firewall Rules for MANGEMENT
resource "google_compute_firewall" "allow-mgmt" {
  name    = "allow-mgmt"
  network = google_compute_network.management.self_link

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["443", "22"]
  }

  source_ranges = ["0.0.0.0/0"]
}

