terraform {
  backend "gcs" {
    credentials = "/var/jenkins_home/cicd-gcp.json"
  #  credentials = "/mnt/credentials/cicd-gcp.json"
    bucket      = "pan-tfstate-cicd"
    prefix      = "terraform/state"
  }
}