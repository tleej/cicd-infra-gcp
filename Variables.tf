// PROJECT Variables
variable "my_gcp_project" {
  default = "cicd-gcp-262611"
}

variable "region" {
  default = "europe-west1"
}

variable "zone" {
  default = "europe-west1-b"
}

variable "zones" {
  description = "for multi zone deployment"
  default = ["europe-west1-b", "europe-west1-c","europe-west1-d"]
}

variable "public_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQCz7/fO6fL7JarcC8VM55K4ceVZKW6jDBbUVGzvwDWB4j0Y0TB29F/DedPsr2HQvthHTG9g2gWSm363ZedeX5xlgy+DcpMm9gtDZqxn14mdpb1F9RcfeRtrgiUrhWCJBIMg9vEKa8/7QGjnKdnBOPWmfac1uy7Ub2YrCjtJ+xZSP7jizpTqMzbVet3QqFz35mPSn7VkPbPcUfv2X9bLbNnsKVnoSBcI/67udPVSrF3MlrPPcr9B2lyyXbGDiZH50SViIlSUlLsHdFLzcWSxlZK0bF1Sr0hOAvevfdHYOCXA3gSMVTog+l53dRi+b4JDk5DLTQSrJXpS+6yBtg4UteM7 admin"
}

variable "serviceaccount" {
  default = "cicd-gcp@cicd-gcp-262611.iam.gserviceaccount.com"
}

// VM-Series Firewall Variables 

variable "firewall_name" {
  default = "firewall"
}

variable "image_fw" {
  # default = "Your_VM_Series_Image"
  
  # /Cloud Launcher API Calls to images/
  # default = "https://www.googleapis.com/compute/v1/projects/paloaltonetworksgcp-public/global/images/vmseries-byol-810"
  # default = "https://www.googleapis.com/compute/v1/projects/paloaltonetworksgcp-public/global/images/vmseries-bundle2-810"
  default = "https://www.googleapis.com/compute/v1/projects/paloaltonetworksgcp-public/global/images/vmseries-bundle1-904"

}

variable "machine_type_fw" {
  default = "n1-standard-4"
}

variable "machine_cpu_fw" {
  default = "Intel Skylake"
}

variable "bootstrap_bucket_fw" {
  default = "pan-bootstrap-gcp-cicd"
}

variable "interface_0_name" {
  default = "management"
}

variable "interface_1_name" {
  default = "untrust"
}

variable "interface_2_name" {
  default = "trust"
}

variable "cluster_name" {
  default = "kubernetes-cluster"
}

variable "scopes_fw" {
  default = ["https://www.googleapis.com/auth/cloud.useraccounts.readonly",
    "https://www.googleapis.com/auth/devstorage.read_only",
    "https://www.googleapis.com/auth/logging.write",
    "https://www.googleapis.com/auth/monitoring.write",
  ]
}
