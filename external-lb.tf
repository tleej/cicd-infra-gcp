resource "google_compute_global_address" "external-address" {
  name = "tf-external-address"
}

resource "google_compute_instance_group" "fw-ig" {
  name = "fw-ig-${count.index + 1}"

  instances = [google_compute_instance.firewall[count.index].self_link]
  
  zone = var.zones[count.index]
  named_port {
    name = "http"
    port = "80"
  }

  count = 2
}

resource "google_compute_health_check" "health-check" {
  name = "elb-health-check"

  http_health_check {
          port = 80
  }
}

resource "google_compute_backend_service" "fw-backend" {
  name     = "fw-backend"
  protocol = "HTTP"

  backend {
    group = google_compute_instance_group.fw-ig[0].self_link
  }

  backend {
    group = google_compute_instance_group.fw-ig[1].self_link
  }

  health_checks = ["${google_compute_health_check.health-check.self_link}"]

}

resource "google_compute_url_map" "http-elb" {
  name            = "http-elb"
  default_service = google_compute_backend_service.fw-backend.self_link

}

resource "google_compute_target_http_proxy" "http-lb-proxy" {
  name    = "tf-http-lb-proxy"
  url_map = google_compute_url_map.http-elb.self_link
}

resource "google_compute_global_forwarding_rule" "default" {
  name       = "http-content-gfr"
  target     = google_compute_target_http_proxy.http-lb-proxy.self_link
  ip_address = google_compute_global_address.external-address.address
  port_range = "80"
}
