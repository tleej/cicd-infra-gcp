// Create a new Palo Alto Networks NGFW VM-Series GCE instance
resource "google_compute_instance" "firewall" {
  name                      = "${var.firewall_name}-${count.index + 1}"
  machine_type              = var.machine_type_fw
  zone                      = var.zones[count.index]
  min_cpu_platform          = var.machine_cpu_fw
  can_ip_forward            = true
  allow_stopping_for_update = true
  count                     = 2

  // Adding METADATA Key Value pairs to VM-Series GCE instance
  metadata = {
    vmseries-bootstrap-gce-storagebucket = var.bootstrap_bucket_fw
    serial-port-enable                   = true
  }

  # ssh-keys                              = "${var.public_key}"

  service_account {
    scopes = var.scopes_fw
  }

    // Swapped Interface for Load Balancer

  network_interface {
    subnetwork = google_compute_subnetwork.untrust-sub.self_link
    access_config {
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.management-sub.self_link
    access_config {
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.trust-sub.self_link
  }

  boot_disk {
    initialize_params {
      image = var.image_fw
    }
  }
}