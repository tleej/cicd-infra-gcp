provider "google" {
  version = "~> 3.3.0"
  credentials = "/var/jenkins_home/cicd-gcp.json"
  # credentials = "/mnt/credentials/cicd-gcp.json"
  project     = var.my_gcp_project
  region      = var.region
  zone        = var.zone
}

provider "google-beta" {
  version = "~> 3.3.0"
  credentials = "/var/jenkins_home/cicd-gcp.json"
#  credentials = "/mnt/credentials/cicd-gcp.json"
  project     = var.my_gcp_project
  region      = var.region
  zone        = var.zone
}

// Adding SSH Public Key Project Wide
resource "google_compute_project_metadata_item" "ssh-keys" {
  key   = "ssh-keys"
  value = var.public_key
}