resource "google_container_cluster" "kubernetes" {
  name               = "k8s-cluster"
  initial_node_count = 1
  location = var.region
  network    = google_compute_network.trust.name
  subnetwork = google_compute_subnetwork.trust-sub.name

  master_auth {
    username = "admin"
    password = "paloaltopaloalto!!"
  }

  node_config {
    oauth_scopes = [
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    tags = ["network-cluster"]
  }

private_cluster_config {

    enable_private_nodes = true
    enable_private_endpoint = false
    master_ipv4_cidr_block = "10.5.0.0/28"
  }

master_authorized_networks_config {

    cidr_blocks {
      cidr_block = "10.0.0.0/8"
      display_name = "JumpHost Access"
    } 
}

ip_allocation_policy {
  
    
}

provisioner "local-exec" {
    command = "gcloud container clusters get-credentials ${google_container_cluster.kubernetes.name} --zone  ${var.region} --project ${var.my_gcp_project}"
}

}

