resource "google_compute_instance" "webserver-1" {
  name         = "webserver-1"
  machine_type = "f1-micro"
  zone         = var.zones[0]
  allow_stopping_for_update = true

  boot_disk {
    initialize_params {
      image = "debian-cloud/debian-9"
    }
  }

  network_interface {
    subnetwork = google_compute_subnetwork.trust-sub.name
  }

  service_account {
    scopes = ["compute-rw"]
  }
  metadata_startup_script = file("startup.sh")
}
