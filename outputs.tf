output "pan-tf-name" {
  value = google_compute_instance.firewall.*.name
}


output "client_certificate" {
  value     = "${google_container_cluster.kubernetes.master_auth.0.client_certificate}"
  sensitive = true
}

output "client_key" {
  value     = "${google_container_cluster.kubernetes.master_auth.0.client_key}"
  sensitive = true
}

output "cluster_ca_certificate" {
  value     = "${google_container_cluster.kubernetes.master_auth.0.cluster_ca_certificate}"
  sensitive = true
}
output "MGT-IP-FW-1" {
  value = google_compute_instance.firewall[0].network_interface.1.access_config.0.nat_ip
}

